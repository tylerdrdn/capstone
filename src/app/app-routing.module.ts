import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceDetailsComponent } from './components/device-details/device-details.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { DeviceComponent } from './components/device/device.component';
import { EmployeeComponent } from './components/employee/employee.component';

const routes: Routes = [
  { path: '', redirectTo: 'employees', pathMatch: 'full' },
  { path: 'employees', component: EmployeeComponent },
  { path: 'employees/:id', component: EmployeeDetailsComponent },
  { path: 'devices', component: DeviceComponent },
  { path: 'devices/:id', component: DeviceDetailsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
