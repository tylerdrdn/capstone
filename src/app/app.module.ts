import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeeComponent } from './components/employee/employee.component';
import { DeviceComponent } from './components/device/device.component';
import { EmployeeCreateComponent } from './components/employee-create/employee-create.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { DeviceCreateComponent } from './components/device-create/device-create.component';
import { DeviceDetailsComponent } from './components/device-details/device-details.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { EmployeeUpdateComponent } from './components/employee-update/employee-update.component';
import { DeviceUpdateComponent } from './components/device-update/device-update.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    EmployeeComponent,
    DeviceComponent,
    EmployeeCreateComponent,
    EmployeeDetailsComponent,
    DeviceCreateComponent,
    DeviceDetailsComponent,
    EmployeeUpdateComponent,
    DeviceUpdateComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
