export class EmployeeDevice {
  id : string;
  description:string;
  employeeId: string;
  constructor(id:string, description:string){
    this.id = id;
    this.description = description;
  }
}
