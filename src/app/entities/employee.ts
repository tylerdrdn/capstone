import { EmployeeDevice } from "./employeedevice";

export class Employee {
  id: string;
  employeeId:Number;
  name : string;
  email: string;
  devices:EmployeeDevice[];

  constructor(id:string, employeeId:Number, name:string,email:string){
    this.id = id;
    this.name = name;
    this.email = email;
  }
}
