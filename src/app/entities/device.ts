export class Device {
  id: string;
  serialNumber: string;
  description: string;
  type: Number;
  typeName: string;
  employeeId: string;
  
  constructor(id: string, serialNumber:string,description:string, type:Number){
    this.id = id;
    this.serialNumber = serialNumber;
    this.description = description;
    this.type = type;
  }
}
