import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../entities/employee';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { Device } from '../entities/device';
import { SelectItem } from '../entities/select-item';

@Injectable({
  providedIn: 'root'
})

export class DeviceService {
  devices: Device[] = [];
  public DevicesCollectionChanged: EventEmitter<void> = new EventEmitter<void>();

  constructor(private db: AngularFireDatabase) {
    //let employee1 = new Employee("1", "empl1", "emp1@gmail.com");
    //let employee2 = new Employee("2", "empl2", "emp2@gmail.com");
    //this.employees.push(employee1,employee2);
  }

  readAll() {
    return new Promise(resolve => {
      this.db.list('Device').snapshotChanges().pipe(map(actions => {
        return actions.map(data => {
          let jsonObj = data.payload.toJSON();
          if(jsonObj){
            jsonObj['id'] = data.key;
          }
          return jsonObj as Device;
        });
      })).subscribe(data => {
        this.devices = data;
        resolve(this.devices);
      })
    })

  }

  getDeviceTypes():SelectItem[] {
      return [
        new SelectItem(1, 'Laptop'),
        new SelectItem(2, 'Screen'),
        new SelectItem(3, 'Mouse'),
        new SelectItem(4, 'Keyboard'),
        new SelectItem(5, 'Mobile'),
        new SelectItem(6, 'Mousepad'),
        new SelectItem(7, 'Camera'),
      ];
    }

    read(id) {
      return new Promise(resolve => {
        let emp = this.db.object('Device/' + id).snapshotChanges().subscribe(data => {
          let jsonObj = data.payload.toJSON();
          if(jsonObj){
            jsonObj['id'] = data.key;
          }
          var device = jsonObj as Device;
          resolve(device);
        });
      })
    }

  readByDeviceId(deviceId) {
    console.log('reading');
    return new Promise(resolve => {
      this.db.list('Device',  ref => ref.orderByChild('deviceId').equalTo(deviceId)).snapshotChanges().pipe(map(actions => {
        return actions.map(data => {
          let a = data.payload.toJSON();
          a['id'] = data.key;
          return a as Device;
        });
      })).subscribe(data => {
        let firstOrDefault = data[0];
        resolve(firstOrDefault);
      })
    })
  }

  create(device) {
    //delete device.id;
    return this.db.list('Device').push(device);
  }

  update(device: Device) {
    var id = device.id;
    //delete device.id;
    var promise = this.db.object('Device/' + id).update(device);
    promise.then(()=>{
      this.DevicesCollectionChanged.emit();
    });
    return promise;
  }

  delete(id) {
    var promise = this.db.object('Device/' + id).remove();
    promise.then(()=>{
      this.DevicesCollectionChanged.emit();
    });
    return promise;
  }
}
