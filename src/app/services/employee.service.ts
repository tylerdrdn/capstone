import { EventEmitter, Injectable } from '@angular/core';
import { Employee } from '../entities/employee';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees: Employee[] = [];
  public CollectionChanged: EventEmitter<void> = new EventEmitter<void>();

  constructor(private db: AngularFireDatabase, private http: HttpClient) {
  }

  getSmartphone(): Observable<HttpResponse<string>> {
    return this.http.get<string>("", { observe: 'response' });
  }


  readAll() {
    return new Promise(resolve => {
      this.db.list('Employee').snapshotChanges().pipe(map(actions => {
        return actions.map(data => {
          let jsonObj = data.payload.toJSON();
          if(jsonObj){
            jsonObj['id'] = data.key;
          }
          return jsonObj as Employee;
        });
      })).subscribe(data => {
        this.employees = data;
        resolve(this.employees);
      })
    })
  }

  read(id) {
    return new Promise(resolve => {
      let emp = this.db.object('Employee/' + id).snapshotChanges().subscribe(data => {
        let jsonObj = data.payload.toJSON();
        if(jsonObj){
          jsonObj['id'] = data.key;
        }
        var emp = jsonObj as Employee;
        console.log(data);
        resolve(emp);
      });
    })
  }

  readByEmployeeId(id) {
    return new Promise(resolve => {
      this.db.list('Employee', ref => ref.orderByChild('employeeId').equalTo(id)).snapshotChanges().pipe(map(actions => {
        return actions.map(data => {
          let a = data.payload.toJSON();
          a['id'] = data.key;
          return a as Employee;
        });
      })).subscribe(data => {
        this.employees = data;
        resolve(this.employees);
      })
    })
  }

  create(emp) {
    //delete emp.id;
    return this.db.list('Employee').push(emp);
  }

  update(emp: Employee) {
    var id = emp.id;
    //delete emp.id;
    var promise = this.db.object('Employee/' + id).update(emp);
    promise.then(() => {
      this.CollectionChanged.emit();
    });
    return promise;
  }

  delete(id) {
    var promise = this.db.object('Employee/' + id).remove();
    promise.then(() => {
      this.CollectionChanged.emit();
    });
    return promise;
  }

  //deleteAll(): Observable<any> {
  //return this.httpClient.delete(baseURL);
  //}

  //searchByName(name): Observable<any> {
  //return this.httpClient.get(`${baseURL}?name=${name}`);
  //}
}
