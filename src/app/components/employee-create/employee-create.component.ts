import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Employee } from 'src/app/entities/employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {
  employee: Employee
  @Output() createItem= new EventEmitter();

  constructor(private employeeService:EmployeeService) {
    this.employee = new Employee('', 0, '', '');
  }

  createEmployee(){
    this.employeeService.create(this.employee).then(()=>{
      this.createItem.emit();

      // clear inputs
      this.employee = new Employee('', 0, '', '');
    });
  }

  ngOnInit(): void {
  }

}
