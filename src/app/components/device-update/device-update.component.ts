import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from 'src/app/entities/device';
import { SelectItem } from 'src/app/entities/select-item';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'app-device-update',
  templateUrl: './device-update.component.html',
  styleUrls: ['./device-update.component.css']
})
export class DeviceUpdateComponent implements OnInit {
  @Input() device: Device;
  @Output() updateDeviceItem= new EventEmitter();
  deviceTypes: SelectItem[];
  @Output() cancelDeviceUpdateItem = new EventEmitter();

  constructor(private deviceService:DeviceService) {
    this.deviceTypes = deviceService.getDeviceTypes();
  }

  updateDevice(){
    this.deviceService.update(this.device).then(()=>{
      this.updateDeviceItem.emit();
    });
  }

  cancel() {
    this.cancelDeviceUpdateItem.emit();
  }

  ngOnInit(): void {
  }

}
