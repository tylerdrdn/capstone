import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Device } from 'src/app/entities/device';
import { SelectItem } from 'src/app/entities/select-item';
import { DeviceService } from 'src/app/services/device.service';
import * as uuid from 'uuid';

@Component({
  selector: 'app-device-create',
  templateUrl: './device-create.component.html',
  styleUrls: ['./device-create.component.css']
})
export class DeviceCreateComponent implements OnInit {
  device: Device;
  deviceTypes: SelectItem[];
  @Output() createDeviceItem= new EventEmitter();

  constructor(private deviceService:DeviceService) {
    console.log('create');
    this.device = new Device('','', '', 0);

    this.deviceTypes = deviceService.getDeviceTypes();
  }

  createDevice(){
    this.deviceService.create(this.device).then(()=>{
      this.createDeviceItem.emit();

      // clear inputs
      this.device = new Device('','', '', 0);
    });
  }

  ngOnInit(): void {
  }

}
