import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Device } from 'src/app/entities/device';
import { Employee } from 'src/app/entities/employee';
import { DeviceService } from 'src/app/services/device.service';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  id: string;
  employee: Employee;
  isDisplaying: boolean = true;
  devices: Device[] = [];

  constructor(private employeeService: EmployeeService,
    private activatedroute: ActivatedRoute,
    private deviceService: DeviceService) {
    this.deviceService.readAll().then((data: any) => {
      this.devices = data;
    });
  }

  changeToEditForm() {
    console.log('test');
    this.isDisplaying = false;
  }

  onCancel($event) {
    this.isDisplaying = true;
  }

  onUpdateItem() {
    this.isDisplaying = true;
    this.refresh();
  }

  ngOnInit(): void {
    this.refresh();
  }

  refresh() {
    this.activatedroute.paramMap.subscribe((param) => {
      this.id = param.get('id');
    })
    this.employeeService.read(this.id).then((data: any) => {
      if(!data)
        return;
        
      if (!data.devices)
        data.devices = [];

      let newArray: any = [];
      for (var prop in data.devices) {
        if (Object.prototype.hasOwnProperty.call(data.devices, prop)) {
          newArray.push(data.devices[prop]);
        }
      }
      data.devices = newArray;
      this.employee = data;
      console.log(this.employee);
    });
  }

}
