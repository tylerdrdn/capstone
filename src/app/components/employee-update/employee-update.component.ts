import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from 'src/app/entities/device';
import { EmployeeDevice } from 'src/app/entities/employeedevice';
import { Employee } from 'src/app/entities/employee';
import { DeviceService } from 'src/app/services/device.service';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css']
})
export class EmployeeUpdateComponent implements OnInit {
  @Input() employee: Employee;
  @Output() updateItem = new EventEmitter();
  @Output() cancelUpdateItem = new EventEmitter();
  devices: Device[] = [];
  //employeedevices: Devicecount[] = [];
  selectedDeviceId: string;

  constructor(private employeeService: EmployeeService, private deviceService: DeviceService) {
    console.log('test');
    this.deviceService.readAll().then((data: any) => {
      this.devices = data.filter(item => item.employeeId == undefined);
    });

  }

  updateEmployee() {
    //let ids = this.employee.devices.map(function(a) {return a.id;});
    this.employeeService.update(this.employee).then(() => {
      this.updateItem.emit();
    });
  }

  addDevice() {
    if (!this.employee.devices) {
      this.employee.devices = [];
    }

    let device = this.devices.find(x => x.id == this.selectedDeviceId);
    if(device)
    {
      // update employee
      this.employee.devices.push(new EmployeeDevice(device.id, device.description));
      this.employeeService.update(this.employee);

      // update decive
      device.employeeId = this.employee.id;
      this.deviceService.update(device);

      // refresh dropdown
      this.devices = this.devices.filter(item => item.employeeId == undefined);
    }
  }

  cancel() {
    this.cancelUpdateItem.emit();
  }

  ngOnInit(): void {
    //let test = this.employee.devices;
    //this.employeedevices.push(new Devicecount("dsad", 23, "desc"));
  }

}
