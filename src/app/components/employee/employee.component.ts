import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/entities/employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
employees:Employee[]=[];

  constructor(private employeeService:EmployeeService) {
    this.refreshData();
  }

  deleteEmp(id:string){
    this.employeeService.delete(id).then(()=>{
      this.refreshData();
    });
  }

  refreshData(){
    this.employeeService.readAll().then((data:any)=>{
      this.employees = data;
    });
  }

  onCreatedItem($event){
    this.refreshData();
  }

  ngOnInit(): void {
     this.employeeService.CollectionChanged.subscribe(() => {
       this.refreshData();
     });
  }

}
