import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Device } from 'src/app/entities/device';
import { SelectItem } from 'src/app/entities/select-item';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.css']
})
export class DeviceDetailsComponent implements OnInit {
  id:string;
  deviceId: Number;
  device: Device;
  isDisplaying: boolean = true;
  deviceTypes: SelectItem[];

  constructor(private deviceService: DeviceService,
    private activatedroute: ActivatedRoute) {
    this.deviceTypes = deviceService.getDeviceTypes();
  }

  changeToEditForm() {
    console.log('test');
    this.isDisplaying = false;
  }

  cancelEditForm() {
    this.isDisplaying = true;
  }

  onCancel($event) {
    this.isDisplaying = true;
  }


  onUpdateItem(){
    this.isDisplaying = true;
    this.refresh();
  }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(){
    this.activatedroute.paramMap.subscribe((param) => {
      this.id = param.get('id');
    })
    this.deviceService.read(this.id).then((data: Device) => {
      if(!data)
        return;
        
      let type =  this.deviceTypes.find(x => x.id == data.type);
      if(type){
        data.typeName = type.name;
      }
      this.device = data;
      console.log(this.device);
    });
  }
}
