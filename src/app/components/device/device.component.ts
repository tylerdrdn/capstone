import { Component, OnInit } from '@angular/core';
import { Device } from 'src/app/entities/device';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {
  devices:Device[]=[];

    constructor(private deviceService:DeviceService) {
      this.refreshData();
    }

    deleteDevice(id:string){
      this.deviceService.delete(id).then(()=>{
        this.refreshData();
      });
    }

    refreshData(){
      this.deviceService.readAll().then((data:any)=>{
        this.devices = data;
      });
    }

    onCreatedItem($event){
      this.refreshData();
    }

    ngOnInit(): void {
       this.deviceService.DevicesCollectionChanged.subscribe(() => {
         this.refreshData();
       });
    }
}
