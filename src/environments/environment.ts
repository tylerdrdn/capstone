// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBpBMiDzhwwsQr-mk35TJO1cS0o9Bd_p9k",
    authDomain: "capstone-fdc34.firebaseapp.com",
    databaseURL: "https://capstone-fdc34-default-rtdb.firebaseio.com",
    projectId: "capstone-fdc34",
    storageBucket: "capstone-fdc34.appspot.com",
    messagingSenderId: "827604411639",
    appId: "1:827604411639:web:4469ff2687b11e48535ff2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
